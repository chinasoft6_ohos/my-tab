/*
 * Copyright (C) 2021 Chinasoft International
 *
 * Licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
    data() {
        return {
            selIdx: this.textSelectedStyle.index,
            itemPadding: 0,
            scrollx: 0,
            itemWidth: [],
            divLeft: 0,
            divTop: 0,
            isShowDiv: false,
            selWidth: 0
        };
    },
    props: {
        fields: {
            default: {
                id: 'id', // id 的字段名
                text: 'text' // 显示文本字段名
            }
        },
        list: {
            default: [], //tab数据，{id:XX,name:XX}为一个item
        },
        tabStyle: { // tab 属性设置
            default: {
                backgroundColor: '#FFFFFF', //背景色
                height: 50, //高度
            }
        },
        textStyle: {
            default: {
                color: '#909399', //未被选中字体颜色
                size: '16px', //未被选中字体大小
            }
        },
        textSelectedStyle: {
            default: {
                index: 0, //默认选中某项 不想要下划线，改属性不设置
                color: '#303133', //选中字体颜色
                size: '18px', //选中字体大小
            }
        },
        showAllNav: {
            default: false
        }
    },
    onLayoutReady() {
        this.toScrollSel(this.textSelectedStyle.index);
        this.resetItemPadding();
        let tabDiv = this.$element("tabDiv").getBoundingClientRect();
        this.selWidth = tabDiv.width / 4;
    },
/**
     * @description item被选中,组件可滚动时，居中
     * @param {Number} index
     */
    toScrollSel(index) {
        let screenwidth = this.$element('mytab').getBoundingClientRect().width; //获取list元素的width
        let listwidth = 0;
        for (let i = 0;i < this.list.length; i++) {
            var rect = this.$element('item' + i).getBoundingClientRect();
            if (this.itemWidth.length > i) {
                if (this.itemWidth[i] < rect.width) this.itemWidth[i] = rect.width;
            } else {
                this.itemWidth.push(rect.width);
            }
            listwidth += this.itemWidth[i];
        }
        console.info("testtest" + screenwidth + "--" + listwidth);

        let width = (screenwidth - this.$element('item' + index).getBoundingClientRect().width) / 2;
        let scrollx = -width;
        for (let i = 0;i < index; i++) {
            scrollx += this.itemWidth[i];
        }
        console.info("testtest" + scrollx + "--" + this.scrollx);
        if (scrollx + screenwidth > listwidth) {
            scrollx = listwidth - screenwidth;
        }
        this.$element("mytab").scrollBy({
            dx: scrollx - this.scrollx
        });
    },
/**
     * 某项tab被点击
     */
    tabClick(index) {
        this.selIdx = index;
        this.$emit("selTab", index);
        this.toScrollSel(index);
    },
    onScroll(e) {
        this.scrollx += e.scrollX;
        //        console.info("testtest"+JSON.stringify(e));
    },
    resetItemPadding() {
        let screenwidth = this.$element('mytab').getBoundingClientRect().width;
        console.log("screenwidth:" + screenwidth);
        let width = 0;
        for (let i = 0;i < this.list.length; i++) {
            width += this.$element('item' + i).getBoundingClientRect().width
        }
        console.log("itemsWidth:" + width);
        if (width < screenwidth && this.itemPadding == 0) {
            this.itemPadding = (screenwidth - width) / this.list.length / 2;
        }
        console.log("screenwidth-itemsWidth:" + this.itemPadding);
    },
    showTabDiv() {
        let position = this.$element("mytab").getBoundingClientRect();
        this.divLeft = position.left;
        this.divTop = position.top;
        this.isShowDiv = !this.isShowDiv;
    },
    tabDivClick(index) {
        this.tabClick(index);
        this.isShowDiv = !this.isShowDiv;
    },
    setSelectIndex(index) {
        this.selIdx = index;
        this.toScrollSel(index);
    },
    stopCapture(e) { // 阻止事件捕获
        console.log(JSON.stringify(e));
    }
}
