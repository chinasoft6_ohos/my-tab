/*
 * Copyright (C) 2021 Chinasoft International
 *
 * Licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
    data() {
        return {
            selIdx: this.textSelectedStyle.index,
            marginRight:0
        }
    },
    props: {
        fields: {
            default: {
                id: 'id', // id 的字段名
                text: 'text' // 显示文本字段名
            }
        },
        list: {
            default: [], //tab数据，{id:XX,name:XX}为一个item
        },
        tabStyle: { // tab 属性设置
            default: {
                backgroundColor: '#FFFFFF', //背景色
                height: 50, //高度
            }
        },
        textStyle: {
            default: {
                color: '#909399', //未被选中字体颜色
                size: '16px', //未被选中字体大小
            }
        },
        textSelectedStyle: {
            default: {
                index: 0, //默认选中某项 不想要下划线，改属性不设置
                color: '#303133', //选中字体颜色
                size: '18px', //选中字体大小
            }
        }
    },
    onInit(){
        this.$watch("textSelectedStyle.index","textSelectedStyleChange")
    },
    textSelectedStyleChange(val){
        this.selIdx=val;
//        this.onLayoutReady();
    },
    selectItem($idx) {
        this.selIdx = $idx;
//        this.onLayoutReady();
    },
    onLayoutReady(){
        let screenwidth = this.$element('tabbar').getBoundingClientRect().width;
        let width = 0;
        for(let i=0;i<this.list.length;i++){
            width += this.$element('item'+i).getBoundingClientRect().width
        }
        if(width<screenwidth && this.marginRight==0){
            this.marginRight = (screenwidth - width)/this.list.length/4;
        }

    }
}