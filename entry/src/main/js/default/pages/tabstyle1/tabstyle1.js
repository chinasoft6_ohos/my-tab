/*
 * Copyright (C) 2021 Chinasoft International
 *
 * Licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
    data: {
        fields: {
            id: 'id',
            text: 'name'
        },
        tabStyle: { // tab 属性设置
            backgroundColor: '#EFE91E63', //背景色
            height: 50, //高度
        },
        textStyle: {
            color: '#eeffff', //未被选中字体颜色
            size: '16px', //未被选中字体大小
        },
        textSelectedStyle: {
            index: 0, // 当前item的index
            color: '#FFFFFF', //选中字体颜色
            size: '18px', //选中字体大小
        },
        list: [
            {
                id: 1,
                name: '精选'
            },
            {
                id: 2,
                name: '大牌疯抢'
            },
            {
                id: 3,
                name: '唯品快抢'
            }
        ],
    },
    onInit() {

    },
    myClick() {

    },
    change(e) {
        this.textSelectedStyle.index = e.index;
    }
}
