# MyTab

### 效果演示
![效果演示](https://images.gitee.com/uploads/images/2021/0814/170716_29eba436_8627638.gif)
### 使用说明
#### 1、list组建的tab组件(灵活)
组件所在文件为js/default/common/component中tabbar。  

1.1、fields属性：id-传进的list对象的用于tab中每个item的id使用的名称，默认"id"；text-传进的list对象的用于tab中每个item的文本展示的名称,默认"text";
icon-点击tab向下图标后展示tab全部选项时，传进的list对象的每个item的图标使用的名称。  
```javascript
 {
      id: 'id',
      text: 'name',
      icon: 'iconUrl'
 }
```

1.2、tab-style属性：backgroundColor-整个tab的背景色，默认"#FFFFFF"；height-tab的高度,默认50。  
```javascript
 { // tab 属性设置
       backgroundColor: '#EFE91E63', //背景色
       height: 50, //高度
 }
```

1.3、text-style属性：color-未被选中字体颜色，即默认字体颜色，默认值”#909399“；size-未被选中字体大小，即默认字体大小，默认值”16px“。  
```javascript
{
      color: '#eeffff', //未被选中字体颜色
      size: '16px', //未被选中字体大小
}
```

1.4、text-selected-style属性：index-默认选中的tab项，从0开始递增计算，默认值0；color-选中tab项字体的颜色，默认值为”#303133“；
size-选中tab项字体大小，默认值”18px“。 
```javascript
{
      index: 0, // 当前item的index
      color: '#FFFFFF', //选中字体颜色
      size: '18px', //选中字体大小
}
```

1.5、list属性，tab展示的数据项，默认值“[]”，其数组item的对象名称要与1.1 fields属性的值相对应。  
```javascript
 [
       {
            id: 1,
            name: '精选',
            iconUrl:"common/icon/image1.png"
       },
       {
            id: 2,
            name: '大牌疯抢',
            iconUrl:"common/icon/image2.png"
       },
       {
             id: 3,
             name: '唯品快抢',
             iconUrl:"common/icon/image1.png"
       }     
 ]
```

1.6、show-all-nav属性，是否含有tab选项平铺展示功能。

1.7、 sel-tab事件，某个tab选项被点击后会触发此事件，传替参数为某个被点击选项的位置，从0开始计算。

1.8、 setSelectIndex方法，参数为int型，某个被选中tab的位置，从0开始计算。  

1.9、使用方式参照js/default/pages下的tabstyle1、js/default/pages下的tabstyle2页面，分别对应属性show-all-nav的false、true的展示效果。

#### 2、tabbar组建的tab组件(不灵活，方便)
组件所在文件为js/default/common/component中mytab。  

2.1、fields属性：id-传进的list对象的用于tab中每个item的id使用的名称，默认"id"；text-传进的list对象的用于tab中每个item的文本展示的名称,默认"text"。  
```javascript
 {
      id: 'id',
      text: 'name'
 }
```

2.2、tab-style属性：backgroundColor-整个tab的背景色，默认"#FFFFFF"；height-tab的高度,默认50。  
```javascript
 { // tab 属性设置
       backgroundColor: '#EFE91E63', //背景色
       height: 50, //高度
 }
```

2.3、text-style属性：color-未被选中字体颜色，即默认字体颜色，默认值”#909399“；size-未被选中字体大小，即默认字体大小，默认值”16px“。  
```javascript
{
      color: '#eeffff', //未被选中字体颜色
      size: '16px', //未被选中字体大小
}
```

2.4、text-selected-style属性：index-默认选中的tab项，从0开始递增计算，默认值0；color-选中tab项字体的颜色，默认值为”#303133“；
size-选中tab项字体大小，默认值”18px“。 
```javascript
{
      index: 0, // 当前item的index
      color: '#FFFFFF', //选中字体颜色
      size: '18px', //选中字体大小
}
```

2.5、list属性，tab展示的数据项，默认值“[]”，其数组item的对象名称要与1.1 fields属性的值相对应。  
```javascript
 [
       {
            id: 1,
            name: '精选'
       },
       {
            id: 2,
            name: '大牌疯抢'
       },
       {
             id: 3,
             name: '唯品快抢'
       }     
 ]
```

2.6、使用方式参照js/default/pages下的tabstyle1页面。

### 实现思路
1、调研  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;查看市面多种app，发现常用菜单大多都是左右滑动菜单，且在滚动许可范围内被选中菜单居中高亮展示。另外，在菜单项较多的时候，右侧还会展示出下拉箭头，点击可将菜单项下拉平铺展示。
总结其大体功能、样式，准备去实现带下拉箭头跟不带下拉箭头的两种tab样式。  

2、确定基础支撑软件  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;鸿蒙有一个tabs（子组件tab-bar、tab-content）组件，可以满足左右滑动菜单效果，只是tabs对子组件有要求，所以实现下拉箭头样式不是那么灵活。此处，可以将tab-bar简单封装下，实现一个左右滑动菜单。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;要实现一个灵活的tab，首先得找到一个左右滑动的基础组件来封装，js没有scrollview组件，我们使用list、list-item（一个）组合替代。到这里，我们就开始动手实现吧。  

3、实现  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1） 使用tab-bar实现一个普通的可滑动tab（common/component/tabbar）,此组件使用有限制，只能作为tabs的子组件使用。tab-bar已经实现了大半的功能，这里我们只要将展示样式加上即可，非常简单不多加叙述。唯一需要注意的是，当菜单项的个数乘以菜单项的宽度小于父元素宽度时，要做下均分处理。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2） 利用list、list-item（一个）实现灵活的tab，需要考虑几方面：横向滚动、选中某项高亮居中效果、下拉层的展示。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;横向滚动将list组件的flex-direction样式设置成row即可。需要注意的是，当菜单项的个数乘以菜单项的宽度小于父元素宽度时，要做下均分处理。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;选中居中高亮居中效果，参照组件js中的toScrollSel方法。将选中的菜单位置传进去，方法会通过一系列计算后利用list的scrollBy方法滚动到正确的位置。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下拉层的展示，采用div的position样式为fixed（相对与整个界面进行定位）覆盖在tab上，并将下拉层窗口下面的区域设置透明度模拟模态弹框。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3） 考虑组件的实用性，暴露出属性供用户设置、事件供用户捕获、方法供用户主动获取tab信息。  

### 仓库地址
[https://gitee.com/chinasoft6_ohos/my-tab](https://gitee.com/chinasoft6_ohos/my-tab)

作者：瞿纬乾、陈巧银
